<?php

/**
 * @file
 *
 * Views will look in this file for any views related hooks.
 * Mainly, this is where you put your hook_views_data()
 * implementation.
 */

/**
 * Implements hook_views_data().
 */
function vjb_views_data() {
  $data = [];

  // Base data.
  $data['vjb']['table']['group'] = t('Json');
  $data['vjb']['table']['base'] = [
    'title' => t('Json'),
    'query_id' => 'vjb',
    'help' => t('Json data provided by the APIs endpoint.'),
  ];

  // Fields.
  $data['vjb']['json_text'] = [
    'title' => t('Json Text'),
    'help' => t('Text in the Json file.'),
    'field' => [
      'id' => 'vjb_standard',
    ],
  ];

  return $data;
}
